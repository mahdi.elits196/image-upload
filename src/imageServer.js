var http = require('http');

var nStatic = require('node-static');

//Path to image storage    
var fullpath = '../../../imagedb/';

var fileServer = new nStatic.Server(fullpath);

http.createServer(function (req, res) {
    
    fileServer.serve(req, res);

}).listen(5000);