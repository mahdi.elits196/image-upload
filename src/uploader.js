var express = require('express');
var moment = require('moment');
var cors = require('cors');
var multer  = require('multer');
var mkdirp = require('mkdirp');
var thumb = require('node-thumbnail').thumb;
var app = express();
var redis = require('redis'),
    client = redis.createClient();

//For cors
app.use(cors());

//Path to image storage    
var fullpath = '../../../imagedb/';

//Path thumb image default
var thumbpath = './thumbs.jpeg';

//Link header 
var linkHeader = 'http://localhost:5000/';

//Key to store image folder list
const FOLDER_LIST_KEY = "folder:list";
const PHOTO_LIST_KEY = "photo:list:";

var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, fullpath + req.query.foldername + '/');
  },
  filename: (req, file, cb) => {
    cb(null, req.query.foldername + '-' + file.fieldname + '-' + Date.now()+ '-' + file.originalname);
  }
})
   
var upload = multer({ storage: storage }).single('image');

//Local function to create api response
function SuccessResponse( responsecode, data){
	return({
			"status" : "success",
			"code" : responsecode,
			"data" : data
	})
}

function ErrorResponse ( responsecode, msg){
	return({
			"status": "error",
			"code" : responsecode,
			"message" : msg
	})
}

//To create folder
app.post('/api/v1/create/folder', (req, res) => {
  var foldername;
  if(req.query.foldername != null|undefined){
    foldername = req.query.foldername;
  }
  else{
    res.send(ErrorResponse(0, "Missing query 'foldername'"));
    return;
  }

  mkdirp( fullpath + req.query.foldername, function(err) {
    if (err) console.error(err)
    else {
      thumb({
        source: thumbpath, 
        destination: fullpath + req.query.foldername,
        width: 150,
        overwrite: true,
        suffix: '',
        basename: "thumbnail"
      }, function(files, err, stdout, stderr) {
        console.log('Thumbnail,done!');
      });
      client.sadd(FOLDER_LIST_KEY, foldername);
      res.send(SuccessResponse(0, {"path" : fullpath + foldername}));
    }
    });
  });

//To post image
app.post('/api/v1/upload/image', upload, (req, res) => {
  console.log(req.file);
  if(req.file != null){
    thumb({
      source: req.file.path, 
      destination: req.file.destination,
      width: 150,
      overwrite: true,
      suffix: '',
      basename: "thumbnail"
    }, function(files, err, stdout, stderr) {
      console.log('Thumbnail,done!');
    });
    client.sadd(PHOTO_LIST_KEY + req.query.foldername, req.file.filename);
    res.send(SuccessResponse(0, req.file));
  }else{
    res.send(ErrorResponse(1, "Error, file is not found"));
  }
});

//To get folder list
app.get('/api/v1/folder/list', (req, res) =>{
  client.scard(FOLDER_LIST_KEY, (err,totalData) => {
    client.smembers(FOLDER_LIST_KEY, (err, response)=>{
      if(err) throw err;
      if (response.length <= 0) {
        res.send(ErrorResponse(1, "Data is empty"));
        return;
      }
      var jsonret = {};
      jsonret["totalData"] = totalData;
      var tempData = [];
      for(let i=0; i<totalData; i++){
        tempData.push({
          backgroundImage: "http://localhost:5000/" + response[i] + "/thumbnail.jpeg",
          backgroundImageUrl: "/photo-list/" + response[i],
          date: "28 February 2019"
        });
      }
      jsonret["userList"] = tempData;
      res.send(SuccessResponse( 0, jsonret));
    });
  });
});

//To insert data
app.post('/api/v1/data', upload, (req, res) => {
  var data = req.query.name;
  console.log(data);
  var key = "data:" +  (moment().format('YYYY[:]MM[:]DD'));
  client.sadd(key, data);
  key = "data:" + (moment().format('YYYY[_]MM[_]DD[_]HH'));
  client.sadd(key, data);

  res.send(SuccessResponse( 0, null));
});

//Get data
app.get('/api/v1/data', upload, (req, res) => {
  var date = req.query.date;
  console.log(date);
  var today = (moment().format('YYYY[:]MM[:]DD'));
  var key = "data:" + today;
  client.scard(key, (err,totalData) => {
    client.smembers(key, (err, response)=>{
      if(err) throw err;
      if (response.length <= 0) {
        res.send(ErrorResponse(1, "Data is empty"));
        return;
      }
      var jsonret = {};
      jsonret["totalData"] = totalData;
      jsonret["date"] = today;
      var tempData = [];
      for(let i=0; i<totalData; i++){
        tempData.push(response[i]);
      }
      jsonret["dataList"] = tempData;
      res.send(SuccessResponse( 0, jsonret));
    });
  });
});

//To insert welcome name
app.post('/api/v1/data/welcome', upload, (req, res) => {
  var data = req.query.name;
  console.log(data);
  var key = "welcome" 
  client.set(key, data);
  client.expire(key, 5);
  res.send(SuccessResponse( 0, null));
});

//Get data welcome
app.get('/api/v1/data/welcome', upload, (req, res) => {

  var key = "welcome";
  client.get(key, (err, ret) => {
    if (ret === null){
      res.send(ErrorResponse(1, ret));
    }
    else res.send(SuccessResponse( 0, ret));
  });
});


//To get photo list
app.get('/api/v1/photo/list', (req, res) =>{
  var foldername = req.query.foldername;
  client.scard(PHOTO_LIST_KEY + foldername, (err,totalData) => {
    client.smembers(PHOTO_LIST_KEY + foldername, (err, response)=>{
      if(err) throw err;
      if (response.length <= 0) {
        res.send(ErrorResponse(1, "Data is empty"));
        return;
      }
      var jsonret = {};
      jsonret["totalData"] = totalData;
      var tempData = [];
      for(let i=0; i<totalData; i++){
        tempData.push({
          src: "http://localhost:5000/" + foldername + "/" + response[i],
          thumbnail: "http://localhost:5000/" + foldername + "/" + response[i],
          thumbnailWidth: 320,
          thumbnailHeight: 180,
          caption: foldername
        });
      }
      jsonret["imageList"] = tempData;
      res.send(SuccessResponse( 0, jsonret));
    });
  });
});

app.listen(4000);