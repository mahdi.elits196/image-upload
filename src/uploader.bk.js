var express = require('express')
var multer  = require('multer')
var app = express();
	
var storage = multer.diskStorage({
	destination: (req, file, cb) => {
	  cb(null, './uploads/')
	},
	filename: (req, file, cb) => {
	  cb(null, file.fieldname + "-" + Date.now() + "-" + file.originalname)
	}
});
 
var upload = multer({storage: storage});
 
app.post('/upload', upload.single("uploadfile"), (req, res) => {
  console.log(req.file);
  res.json({'msg': 'File uploaded successfully!', 'file': req.file});
});

app.listen(3000);